<?php

/**
 * @file
 * Contains Drupal\disclaimerconsent\Form\SettingsForm.
 */

namespace Drupal\disclaimerconsent\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\disclaimerconsent\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'disclaimerconsent.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disclaimerconsent_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('disclaimerconsent.settings');

    
    $form['theme'] = [
      '#type' => 'select',
      '#options' => [
        'none' => $this->t('- None -'),
        'dark-top' => $this->t('Dark Top'),
        'dark-floating' => $this->t('Dark Floating'),
        'dark-bottom' => $this->t('Dark Bottom'),
        'light-floating' => $this->t('Light Floating'),
        'light-top' => $this->t('Light Top'),
        'light-bottom' => $this->t('Light Bottom'),
      ],
      '#title' => $this->t('Choose your theme'),
      '#description' => $this->t('Select the theme you wish to use.'),
      '#default_value' => $config->get('theme'),
    ];
   
    $form['texts'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => $this->t('Custom texts'),
    ];
   
    $form['texts']['headline_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Headline Text'),
      '#description' => $this->t('The message shown by the plugin.'),
      '#rows' => 2,
      '#default_value' => $config->get('headline_text'),
    ];
    $form['texts']['accept_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Accept button text'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('accept_button_text'),
    
    ];
    $form['texts']['decline_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Decline button text'),
      '#description' => $this->t('The text shown on the link to the cookie policy (requires the Cookie policy option to also be set)'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('decline_button_text'),
     
    ];
   
    $form['expiry'] = [
      '#type' => 'number',
      '#title' => $this->t('Expiry days'),
      '#description' => $this->t('The number of days Cookie Consent should store the user’s consent information for.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('expiry'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('disclaimerconsent.settings')
      ->set('theme', $form_state->getValue('theme'))
      ->set('headline_text', $form_state->getValue('headline_text'))
      ->set('accept_button_text', $form_state->getValue('accept_button_text'))
      ->set('decline_button_text', $form_state->getValue('decline_button_text'))
      ->set('expiry', $form_state->getValue('expiry'))
	->save();
  }

}
